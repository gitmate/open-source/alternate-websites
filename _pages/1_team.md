---
layout: page
title: Team
permalink: /team/
---

We are a diverse team compiled out of developers and founders.

Here's a couple of us:

<div class="row">
  <div class="col s12 m4">
    <a href="https://www.linkedin.com/in/fabian-neuschmidt-508b53159/" target="_blank">
      <div class="author">
        <img src="../assets/img/team/Fabian.jpg" alt="Fabian">
        <div class="author-details">
          <div class="name">Fabian Neuschmidt</div>
          <div class="position">Data Scientist</div>
        </div>
      </div>
    </a>
    <p>
      Fabian is the CTO of <a href="https://gitmate.io/" target="_blank">GitMate.io</a>
      and acquired awesome data analysis skillz.
    </p>
  </div>
  <div class="col s12 m4">
    <a href="https://www.linkedin.com/in/sebastian-latacz-246882b7/" target="_blank">
      <div class="author">
        <img src="../assets/img/team/Sebastian.jpg" alt="Sebastian">
        <div class="author-details">
          <div class="name">Sebastian Latacz</div>
          <div class="position">Backend Developer</div>
        </div>
      </div>
    </a>
    <p>
      Sebastian codes hard in our Hamburg office.
      If an important deadline hits soon, Sebastian is the hero you need - when
      we got started on apps, he wrote one in just under two hours.
    </p>
  </div>
  <div class="col s12 m4">
    <a href="https://www.linkedin.com/in/lasse-schuirmann-5395a7123/" target="_blank">
      <div class="author">
        <img src="../assets/img/team/Lasse.png" alt="Lasse">
        <div class="author-details">
          <div class="name">Lasse Schuirmann</div>
          <div class="position">Whatever is Needed</div>
        </div>
      </div>
    </a>
    <p>
      After his computer science studies, Lasse founded
      <a href="https://viperdev.io/">ViperDev.io</a> as well as
      <a href="https://gitmate.io/" target="_blank">GitMate.io</a> to help startups and enterprises
      build better software.
    </p>
  </div>
</div>

If you want to meet us in person at our Hamburg office, shoot us an email at
<a href="mailto:contact@hamburg-data-science.de">contact@hamburg-data-science.de</a>.
